package friscilla.lely.appx08

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    val RC_SUKSES : Int = 100
    var bgH : String = "YELLOW"
    var fHead : Int = 30
    var fTitle : Int = 30
    var bg : String = "WHITE"
    var isi : String = "Yowis Ben merupakan film drama-komedi Indonesia yang dirilis pada 22 Februari 2018. Film ini dibintangi oleh Bayu Skak, Brandon Salim, Cut Meyriska, Joshua Suherman, serta Tutus Thomson. Disutradarai oleh Fajar Nugros dan Bayu Skak, film ini 80% menggunakan bahasa Jawa."
    lateinit var pref : SharedPreferences
    val PREF_NAME = "Setting"
    val FIELD_FONT_HEADER = "Font_Size_Header"
    val FIELD_FONT_TITLE = "Font_Size_Title"
    val FIELD_BACK = "BACKGROUND"
    val FIELD_BACKHEAD = "BACKGROUND_HEADER"
    val FIELD_TEXT = "text"
    val DEF_FONT_HEADER = fHead
    val DEF_FONT_TITLE = fTitle
    val DEF_BACK = bg
    val DEF_BACKHEAD = bgH
    val DEF_ISI = isi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txtIsi.setText(pref.getString(FIELD_TEXT,DEF_ISI))
        fHead = pref.getInt(FIELD_FONT_HEADER,DEF_FONT_HEADER)}}


